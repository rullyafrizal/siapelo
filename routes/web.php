<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\ControllerInitialController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\EventLogController;
use App\Http\Controllers\FacilityController;
use App\Http\Controllers\FacilityLogController;
use App\Http\Controllers\ImagesController;
use App\Http\Controllers\LogShiftController;
use App\Http\Controllers\OperationalLogController;
use App\Http\Controllers\PersonnelLogController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('logout', [AuthenticatedSessionController::class, 'destroy'])
    ->name('logout');

Route::group(['middleware' => 'guest'], function () {
    // Auth
    Route::get('login', [AuthenticatedSessionController::class, 'create'])
        ->name('login')
        ->middleware('guest');

    Route::post('login', [AuthenticatedSessionController::class, 'store'])
        ->name('login.store')
        ->middleware('guest');
});

Route::group(['middleware' => 'auth'], function () {
    // Operational Logs
    Route::resource('operational-logs', OperationalLogController::class)
        ->only(['store', 'index', 'update', 'create', 'edit', 'destroy']);

    Route::get('operational-logs/{operational_log}/action/download', [OperationalLogController::class, 'export'])
        ->name('operational-logs.export');

    // Dashboard
    Route::get('/', [DashboardController::class, 'index'])
        ->name('dashboard');

    // Role
    Route::resource('roles', RoleController::class);

    // Facilities
    Route::resource('facilities', FacilityController::class);
    Route::put('facilities/{facility}/restore', [FacilityController::class, 'restore'])->name('facilities.restore');

    // Log Shifts
    Route::resource('log-shifts', LogShiftController::class);
    Route::put('log-shifts/{log_shift}/restore', [LogShiftController::class, 'restore'])->name('log-shifts.restore');

    // Log Shifts
    Route::resource('positions', PositionController::class);
    Route::put('positions/{position}/restore', [PositionController::class, 'restore'])->name('positions.restore');

    // Users
    Route::resource('users', UserController::class);
    Route::put('users/{user}/restore', [UserController::class, 'restore'])
        ->name('users.restore');

    // Operational Logs
    Route::resource('operational-logs', OperationalLogController::class)->except(['store', 'edit', 'update']);
    Route::put('operational-logs/{operational_log}/restore', [OperationalLogController::class, 'restore'])->name('operational-logs.restore');
    Route::put('operational-logs/{operational_log}/review', [OperationalLogController::class, 'review'])->name('operational-logs.review');
    Route::get('operational-logs/{operational_log}/ready-to-review', [OperationalLogController::class, 'readyToReview'])->name('operational-logs.ready-to-review');
    Route::get('operational-logs/{operational_log}/export-to-pdf', [OperationalLogController::class, 'export'])->name('operational-logs.export-to-pdf');

    // Event Logs
    Route::resource('event-logs', EventLogController::class)->except(['edit', 'update', 'destroy', 'create']);
    Route::get('event-logs/{operational_log}/edit', [EventLogController::class, 'edit'])
        ->name('event-logs.edit');
    Route::put('event-logs/{operational_log}', [EventLogController::class, 'update'])
        ->name('event-logs.update');

    // Facility Logs
    Route::resource('facility-logs', FacilityLogController::class)->except(['edit', 'update', 'destroy', 'store', 'show']);
    Route::get('facility-logs/{operational_log}/edit', [FacilityLogController::class, 'edit'])
        ->name('facility-logs.edit');
    Route::put('facility-logs/{operational_log}', [FacilityLogController::class, 'update'])
        ->name('facility-logs.update');
    Route::post('facility-logs/{operational_log}', [FacilityLogController::class, 'store'])
        ->name('facility-logs.store');
    Route::get('facility-logs/export-xls', [FacilityLogController::class, 'exportXls'])
        ->name('facility-logs.export-xls');

    // Personnel Logs
    Route::resource('personnel-logs', PersonnelLogController::class)->except(['edit', 'update', 'destroy']);
    Route::get('personnel-logs/{operational_log}/edit', [PersonnelLogController::class, 'edit'])
        ->name('personnel-logs.edit');
    Route::put('personnel-logs/{operational_log}', [PersonnelLogController::class, 'update'])
        ->name('personnel-logs.update');
});

// Images
Route::get('/img/{path}', [ImagesController::class, 'show'])
    ->where('path', '.*')
    ->name('image');
