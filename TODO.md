# TODO
- butuh bikin mekanisme supaya gaada log_date dan log_shift yang duplikat -> [DONE]
- bikin Mekanisme untuk auto reject operational log yang lebih dari 72 jam dan belum ada review -> [DONE]
- Perlu bikin button ready to review yang bisa diklik setelah operational log udah siap direview (tombol toggle) -> [DONE]
- Perlu bikin seeder untuk data master facilities, positions, dan log shifts -> [DONE]
- Decompose controller initials -> [DONE]
- Bikin fitur export laporan operational logs -> [DONE]
- Tambahkan fitur tanda tangan ketika approve operational log -> [DONE]
- Bikin akun staff, manajer sesuai dengan role dan permissionnya -> [DONE]
- Perlu tambahan notifikasi ketika log approved and rejected -> [DONE]
- Tambahkan disabled update di event logs, facility logs,personnel logs ketika log sudah diapprove atau lewat 72 jam -> [DONE]
- Perbaikin tampilan facilities di detail operational logs -> [DONE]
- Bikin Dashboard -> [DONE]
- Fix kenapa ketika reject operational log modalnya gak otomatis ketutup -> [DONE]
- Ubah cara nyimpen created_by dan updated_by di operational_logs dari foreign key ke json -> [DONE]
- Bikin mekanisme buat soft delete di operational logs (deleted_at dan deleted by) -> [DONE]

## Constraints
- Log tidak akan bisa diupdate setelah 72 jam dihitung dari created_at termasuk: (Master operational logs, Event Logs, Facility Logs, Personnel Logs)
- Approval/Rejection (Review) tidak akan bisa dilakukan setelah 72 jam dihitung dari created_at
- Hanya admin saja yang bisa melakukan penghapusan log dari sistem

## Flow Review
- Staff membuat operational log dan mengisi
- Setelah staff klik tombol ready to review, scheduler akan mengirim reminder review ke manajer
- Jika manajer approve, done
- Jika manajer reject, sistem akan mengirim notifikasi ke creator operational log untuk merevisi
- Staff merevisi dan klik tombol ready to review lagi, scheduler akan mengirim reminder review ke manajer
- Manager approve, done

## Button Ready to Review requirement
- Bisa diklik kapan aja, once the operational log (master) udah dibuat, bisa diklik
- Tidak bisa setelah 72 jam

## Mekanisme reminder review
- Reminder dikirim ketika user turn on toggle ready to review
- Reminder dikirim ke user dengan positions ATS Operations Manager

## Breakdown Fitur Export
- Siapin dependencies library
- Bikin template HTML
- Bikin service buat export pdf
- Bikin endpoint buat export pdf

## Fitur notifikasi ketika approved dan rejected
- Perlu kirim email ke staff yang jadi personnel on duty dalam log tersebut
- Prerequisitenya harus punya data user di personnel on duty

## Isi dashboard
- Jumlah keseluruhan operational logs
- Jumlah operational logs yang sudah diapprove
- Jumlah operational logs yang rejected
- Jumlah operational logs yang need approval
- Jumlah User tiap position









======================================
# TODO batch 2
- Operational log hanya bisa dilihat oleh pembuatnya/yang masuk dalam personnel on duty
- Perlu bikin konsep baru soal facility log
- Perlu bikin konsep baru soal event log
- Bikin rekap excel yang bisa dipilih time range (mirip rekap dotify)
- Halaman personnel on duty sesimpel karyawan absen saja
- Perlu fixing kenapa v-model gak kebinding setelah validation error

- Refactor operational log controller

## Konsep baru operational log -> [DONE]
- Halaman create dihilangkan
- Halaman index ada tombol create operational log kalau dalam waktu itu belum ada operational log di shift itu
- Tombol create ini nanti akan generate otomatis oplog

## Konsep baru Personnel on Duty -> [DONE]
- Karyawan masuk ke halaman pod
- karyawan tekan tombol absen
- otomatis terdaftar sebagai pod di hari itu dan shift itu
- karyawan bisa lihat dia terdaftar di operational log mana saja dan masuk jam berapa

## Rekap excel -> [DONE]
- rekap excel berisi daftar kondisi fasilitas di hari itu
### Breakdown
- User bisa export facility logs dengan bentuk excel
- User bisa pilih date range untuk facility logs-nya
- Akan ada log date dan kondisi fasilitas di 
- Log Date | Facility A

## Konsep baru event log -> [DONE]
- User hanya bisa create event log ketika dia sedang berada di sebuah shift -> [DONE]
- Event log akan memiliki siapa yang mengisi, waktu inputan, dan waktu created at -> [DONE]
- Tiap event log hanya bisa diupdate/dihapus oleh orang yang membuatnya saja -> [DONE]
- Ada event log yang otomatis made by system -> [DONE]
- Bikin validasi time juga di BE, gaboleh di luar jam shift -> [DONE]
### Event log otomatis made by system
- Ketika user create operational log -> John Doe created operational log [06:00] -> [DONE]
- Ketika user clock in ke sebuah operational log -> Sarah Doe clock in operational log (06:30) [DONE]
- Ketika user menambahkan facility inspection (create facility log) -> User X created facility log (07:00) -> [DONE]
- Ketika user edit facility log -> User X updated facility log for facility X from state A to state B (09:30) -> [DONE]

## Konsep baru facility log -> [DONE]
- User hanya bisa nambahin facility log ketika dia sedang berada di sebuah shift
- Facility log punya keterangan last updated by
- Form facility log statusnya diperbanyak dan dimodel jadi dropdown

## Dashboard Batch 2 -> [DONE]
- Admin bisa lihat semua jumlah operational logs dan bisa lihat jumlah user
- Manager bisa lihat semua jumlah operational logs dan tidak bisa lihat jumlah user
- Staff hanya bisa lihat jumlah operational log dia sendiri dan tidak bisa lihat jumlah user


