<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (config('permission.permission_list') as $listPermission) {
            foreach ($listPermission as $permission) {
                Permission::query()->updateOrCreate([
                    'name' => $permission,
                ]);
            }
        }
    }
}
