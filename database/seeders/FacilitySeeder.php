<?php

namespace Database\Seeders;

use App\Models\Facility;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FacilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $facilities = [
            'Phone Tele', 'Phone Coord', 'HT 3', 'HT 1', 'HT 2',
            'IAIS', 'AFTN', 'AFLS', 'FIDS', 'Navaid Monitor', 'VSCS',
            'Binocular', 'Sirine', 'Crash Bell', 'Signal Lamp', 'AWOS',
            'RX 120.55', 'TX 120.55', 'RX 122.4', 'TX 122.4'
        ];

        foreach ($facilities as $facility) {
            Facility::query()
                ->updateOrCreate([
                    'name' => $facility
                ]);
        }
    }
}
