<?php

namespace Database\Seeders;

use App\Models\LogShift;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LogShiftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $logshifts = [
            [
                'name' => 'PAGI',
                'start_time' => '00:00:00',
                'end_time' => '08:00:00'
            ],
            [
                'name' => 'SIANG',
                'start_time' => '08:00:00',
                'end_time' => '16:00:00'
            ],
            [
                'name' => 'MALAM',
                'start_time' => '16:00:00',
                'end_time' => '23:59:59'
            ],
        ];

        foreach ($logshifts as $shift) {
            LogShift::query()
                ->updateOrCreate(['name' => $shift['name']], $shift);
        }
    }
}
