<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET CONSTRAINTS ALL DEFERRED;');
        DB::table('roles')->truncate();
        DB::table('permissions')->truncate();
        DB::table('role_has_permissions')->truncate();
        DB::table('model_has_roles')->truncate();
        DB::table('model_has_permissions')->truncate();
        DB::statement('SET CONSTRAINTS ALL IMMEDIATE;');

        $this->call(PermissionSeeder::class);

        $role = Role::where('name', 'Admin')->firstOrCreate([
            'name' => 'Admin'
        ]);

        if ($role && $user = User::where('first_name', 'Admin')->first()) {
            $role->givePermissionTo('full-access');
            $user->assignRole($role);
        }
    }
}
