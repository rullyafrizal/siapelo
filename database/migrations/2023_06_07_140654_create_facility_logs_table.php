<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facility_logs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('time')->nullable();
            $table->string('name');
            $table->string('status');
            $table->string('description')->nullable();
            $table->uuid('operational_log_id')->nullable(true);
            $table->jsonb('created_by')->nullable();
            $table->jsonb('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facility_logs');
    }
};
