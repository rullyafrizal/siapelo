<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personnel_logs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->jsonb('personnel_detail')->nullable();
            $table->boolean('sign');
            $table->time('time');
            $table->timestamps();
            $table->uuid('operational_log_id')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personnel_logs');
    }
};
