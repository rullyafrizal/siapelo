<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operational_logs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->date('log_date')->nullable();
            $table->string('shift_name')->nullable();
            $table->time('shift_start_time')->nullable();
            $table->time('shift_end_time')->nullable();
            $table->boolean('log_time')->nullable();
            $table->boolean('log_sign')->nullable();
            $table->boolean('ready_to_review')->nullable();
            $table->string('status')->nullable();
            $table->dateTime('last_review_reminder_sent_at')->nullable();
            $table->text('reviewer_signature')->nullable();
            $table->jsonb('updated_by')->nullable();
            $table->jsonb('created_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->json('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operational_logs');
    }
};
