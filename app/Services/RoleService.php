<?php

namespace App\Services;

use Spatie\Permission\Models\Permission;

class RoleService {
    public function getPermissions()
    {
        return [
            'permissions' => [
                [
                    'title' => 'All Permissions',
                    'list' => Permission::query()
                        ->whereNotIn('name', config('permission.permission_list.full_access_permission'))
                        ->get('name')
                ],
                [
                    'title' => 'Full Access Permission',
                    'list' => Permission::query()
                        ->whereIn('name', config('permission.permission_list.full_access_permission'))
                        ->get('name')
                ],
                [
                    'title' => 'Dashboard Permission',
                    'list' => Permission::query()
                        ->whereIn('name', config('permission.permission_list.dashboard_permissions'))
                        ->get('name')
                ],
                [
                    'title' => 'User Permissions',
                    'list' => Permission::query()
                        ->whereIn('name', config('permission.permission_list.user_permissions'))
                        ->orderBy('name')
                        ->get('name')
                ],
                [
                    'title' => 'Role Permissions',
                    'list' => Permission::query()
                        ->whereIn('name', config('permission.permission_list.role_permissions'))
                        ->orderBy('name')
                        ->get('name')
                ],
                [
                    'title' => 'Facility Permissions',
                    'list' => Permission::query()
                        ->whereIn('name', config('permission.permission_list.facility_permissions'))
                        ->orderBy('name')
                        ->get('name')
                ],
                [
                    'title' => 'Log Shift Permissions',
                    'list' => Permission::query()
                        ->whereIn('name', config('permission.permission_list.log_shift_permissions'))
                        ->orderBy('name')
                        ->get('name')
                ],
                [
                    'title' => 'Position Permissions',
                    'list' => Permission::query()
                        ->whereIn('name', config('permission.permission_list.position_permissions'))
                        ->orderBy('name')
                        ->get('name')
                ],
                [
                    'title' => 'Operational Logs Permissions',
                    'list' => Permission::query()
                        ->whereIn('name', config('permission.permission_list.operational_logs_permissions'))
                        ->orderBy('name')
                        ->get('name')
                ],
                [
                    'title' => 'Event Logs Permissions',
                    'list' => Permission::query()
                        ->whereIn('name', config('permission.permission_list.event_logs_permissions'))
                        ->orderBy('name')
                        ->get('name')
                ],
                [
                    'title' => 'Facilities Logs Permissions',
                    'list' => Permission::query()
                        ->whereIn('name', config('permission.permission_list.facilities_logs_permissions'))
                        ->orderBy('name')
                        ->get('name')
                ],
                [
                    'title' => 'Personnel Logs Permissions',
                    'list' => Permission::query()
                        ->whereIn('name', config('permission.permission_list.personnel_logs_permissions'))
                        ->orderBy('name')
                        ->get('name')
                ]
            ]
        ];
    }
}
