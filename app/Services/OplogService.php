<?php

namespace App\Services;

use App\Enums\DateFormat;
use App\Enums\ReviewStatus;
use App\Enums\RoleEnum;
use App\Http\Requests\OperationalLog\ReviewOperationalLogRequest;
use App\Http\Resources\LogShift\LogShiftResource;
use App\Jobs\ReviewLogReminder;
use App\Jobs\SendReviewResult;
use App\Models\EventLog;
use App\Models\LogShift;
use App\Models\OperationalLog;
use App\Models\PersonnelLog;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class OplogService
{
    public function createOperationalLog()
    {
        return DB::transaction(function () {
            $auth = json_encode([
                'id' => auth()->id(),
                'first_name' => auth()->user()->first_name,
                'last_name' => auth()->user()->last_name,
                'email' => auth()->user()->email
            ]);

            $shift = new LogShiftResource(
                LogShift::query()
                    ->where('start_time', '<=', nowInHisFormat())
                    ->where('end_time', '>=', nowInHisFormat())
                    ->first()
            );

            $duplicateCount = OperationalLog::query()
                ->whereDate('log_date', '=', Carbon::now()->format(DateFormat::ONLY_DATE))
                ->where('shift_name', '=', $shift->name)
                ->count();

            if ($duplicateCount > 0) {
                return redirect()
                    ->route('operational-logs.index')
                    ->with('error', 'Operational log has been created before!');
            }

            $oplog = OperationalLog::query()
                ->create([
                    'log_date' => Carbon::now()->format(DateFormat::ONLY_DATE),
                    'shift_name' => $shift->name,
                    'shift_start_time' => $shift->start_time,
                    'shift_end_time' => $shift->end_time,
                    'created_by' => $auth,
                    'updated_by' => $auth,
                    'ready_to_review' => false,
                    'status' => ReviewStatus::IN_PROGRESS
                ]);

            $isStaff = !(auth()->user()->hasRole(RoleEnum::ADMIN) || auth()->user()->hasRole(RoleEnum::MANAGER));
            if ($isStaff) {
                PersonnelLog::query()
                    ->create([
                        'id' => (string) Str::uuid(),
                        'time' => nowInHisFormat(),
                        'personnel_detail' => $auth,
                        'sign' => true,
                        'operational_log_id' => $oplog->id,
                        'created_at' => now(),
                        'updated_at' => now()
                    ]);
                EventLog::query()
                    ->create([
                        'time' => nowInHisFormat(),
                        'specification' => 'Personnel ' . auth()->user()->first_name . ' ' . auth()->user()->last_name . ' clock in',
                        'operational_log_id' => $oplog->id,
                        'created_by' => json_encode([
                            'id' => 'SYSTEM',
                            'first_name' => 'SYSTEM',
                            'last_name' => 'SYSTEM',
                            'email' => 'SYSTEM'
                        ]),
                        'updated_by' => json_encode([
                            'id' => 'SYSTEM',
                            'first_name' => 'SYSTEM',
                            'last_name' => 'SYSTEM',
                            'email' => 'SYSTEM'
                        ])
                    ]);
            }

            EventLog::query()
                ->create([
                    'time' => nowInHisFormat(),
                    'specification' => 'Operational log has been created by ' . auth()->user()->first_name . ' ' . auth()->user()->last_name,
                    'operational_log_id' => $oplog->id,
                    'created_by' => json_encode([
                        'id' => 'SYSTEM',
                        'first_name' => 'SYSTEM',
                        'last_name' => 'SYSTEM',
                        'email' => 'SYSTEM'
                    ]),
                    'updated_by' => json_encode([
                        'id' => 'SYSTEM',
                        'first_name' => 'SYSTEM',
                        'last_name' => 'SYSTEM',
                        'email' => 'SYSTEM'
                    ])
                ]);

            return $shift;
        });
    }

    public function review(OperationalLog $operationalLog, ReviewOperationalLogRequest $request)
    {
        return DB::transaction(function() use ($operationalLog, $request) {
            $operationalLog->update([
                'ready_to_review' => false,
                'status' => $this->determineStatus($request),
                'last_review_reminder_sent_at' =>
                    $this->determineStatus($request) == ReviewStatus::REJECTED ?
                        null : $operationalLog->last_review_reminder_sent_at,
                'reviewer_signature' => $this->determineStatus($request) == ReviewStatus::APPROVED ?
                    $request->get('reviewer_signature') : null
            ]);
            $operationalLog->reviewHistories()
                ->insert([
                    'id' => (string) Str::uuid(),
                    'reviewer_notes' => $request->get('reviewer_notes'),
                    'reviewed_by' => json_encode([
                        'id' => auth()->id(),
                        'first_name' => auth()->user()->first_name,
                        'last_name' => auth()->user()->last_name,
                        'email' => auth()->user()->email
                    ]),
                    'reviewed_at' => now(),
                    'status' => $this->determineStatus($request),
                    'operational_log_id' => $operationalLog->id,
                ]);

            $updatedOperationalLog = $operationalLog->fresh();
            $this->sendReviewResultEmail($updatedOperationalLog);
        });
    }

    public function sendReviewResultEmail(OperationalLog $operationalLog) {
        $personnelLogs = $operationalLog->personnelLogs()->get();
        $staffEmails = [];
        foreach ($personnelLogs as $personnel_log) {
            $personnel_detail = json_decode($personnel_log->personnel_detail);
            if ($personnel_detail == null) break;

            $staffEmails[] = $personnel_detail->email;
        }

        $notifiables = User::query()
            ->whereIn('email', $staffEmails)
            ->get();
        dispatch(new SendReviewResult($operationalLog, $notifiables));
    }

    public function readyToReview(OperationalLog $operationalLog) {
        return DB::transaction(function() use ($operationalLog) {
            if ($operationalLog->status == ReviewStatus::APPROVED) {
                return redirect()
                    ->route('operational-logs.index')
                    ->with('error', "No more changes allowed after log has been approved!");
            }

            $updatedStatus = !$operationalLog->ready_to_review;
            $lastReview = $operationalLog->reviewHistories()
                ->orderByDesc('reviewed_at')
                ->first();
            $turnOffStatus = ReviewStatus::IN_PROGRESS;
            if ($lastReview) {
                $turnOffStatus = $lastReview->status;
            }

            $operationalLog->update([
                'ready_to_review' => $updatedStatus,
                'status' => $updatedStatus ? ReviewStatus::NEED_APPROVAL : $turnOffStatus,
            ]);

            if ($updatedStatus) {
                dispatch(new ReviewLogReminder($operationalLog));
            }
        });
    }

    public function determineStatus(ReviewOperationalLogRequest $request): string
    {
        return $request->get('status') == 'approve' ? ReviewStatus::APPROVED : ReviewStatus::REJECTED;
    }

    public function exportPdf(OperationalLog $operationalLog) {
        $reviewerName = 'SYSTEM';
        $reviewedAt = null;
        $eventLogsDTO = [];
        $personnelLogsDTO = [];
        $facilitiesDTO = [];

        // Review History
        $reviewHistory = $operationalLog->reviewHistories()
            ->orderByDesc('reviewed_at')
            ->first();
        if ($reviewHistory) {
            $reviewer = json_decode($reviewHistory->reviewed_by);
            $reviewerName = $reviewHistory->name ?? $reviewer->first_name . ' ' . $reviewer->last_name;
            $reviewedAt = $reviewHistory->reviewed_at;
        }

        // Event Logs
        $event_logs = $operationalLog->eventLogs()
            ->orderByDesc('created_at')
            ->get();
        if ($event_logs) {
            foreach ($event_logs as $event) {

                $eventLogsDTO[] = [
                    'time' => to_carbon($event->time)->format(DateFormat::HOUR_MINUTE_SEC),
                    'specification' => $event->specification
                ];
            }
        }

        // Personnel Logs
        $personnel_logs = $operationalLog->personnelLogs()
            ->orderByDesc('created_at')
            ->get();
        if ($personnel_logs) {
            foreach ($personnel_logs as $personnel) {
                $personnelLogsDTO[] = [
                    'time' => $personnel->time,
                    'name' => json_decode($personnel->personnel_detail)->first_name . ' ' . json_decode($personnel->personnel_detail)->last_name,
                    'sign' => $personnel->time
                ];
            }
        }

        // Facilities
        $facilities = $operationalLog->facilityLogs()
            ->orderBy('name')
            ->get();
        if ($facilities) {
            foreach ($facilities as $facility) {
                $facilitiesDTO[] = [
                    'name' => $facility->name,
                    'status' => $facility->status,
                    'description' => $facility->description
                ];

            }
        }

        $oplog = [
            'data' => [
                'reviewerSignature' => $operationalLog->reviewer_signature,
                'reviewedAt' => to_carbon($reviewedAt)->format(DateFormat::WITH_TIME),
                'logDate' => $operationalLog->log_date,
                'logShift' => $operationalLog->shift_name . " (" . format_time($operationalLog->shift_start_time) . "-" . format_time($operationalLog->shift_end_time) . ")",
                'reviewerName' => $reviewerName,
                'eventLogs' => $eventLogsDTO,
                'personnelLogs' => $personnelLogsDTO,
                'facilities' => $facilitiesDTO
            ]
        ];

        return view('downloads.operational_log.export_log', compact('oplog'));
    }
}
