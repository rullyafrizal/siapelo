<?php

namespace App\Models;

use App\Traits\UseUuid;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class OperationalLog extends Model
{
    use HasFactory,
        UseUuid,
        SoftDeletes;

    protected $guarded = [];

    public function eventLogs(): HasMany
    {
        return $this->hasMany(EventLog::class);
    }

    public function personnelLogs(): HasMany
    {
        return $this->hasMany(PersonnelLog::class);
    }

    public function reviewHistories(): HasMany
    {
        return $this->hasMany(ReviewHistory::class);
    }

    public function facilityLogs(): HasMany
    {
        return $this->hasMany(FacilityLog::class);
    }

    public function resolveRouteBinding($value, $field = null)
    {
        return $this->where($field ?? 'id', $value)->withTrashed()->firstOrFail();
    }

    public function scopeFilters(Builder $query, array $filters = [])
    {
        return $query
            ->when($filters['search'] ?? null, function (Builder $query, $search) {
                return $query->whereRaw("created_by->>'first_name' ILIKE ?", ["%{$search}%"])
                    ->orWhereRaw("created_by->>'last_name' ILIKE ?", ["%{$search}%"]);
            })
            ->when($filters['periodStart'] ?? null, function (Builder $query, $periodStart) {
                return $query->whereDate('log_date', '>=', $periodStart);
            })
            ->when($filters['periodEnd'] ?? null, function (Builder $query, $periodEnd) {
                return $query->whereDate('log_date', '<=', $periodEnd);
            })
            ->when($filters['work_shift'] ?? null, function (Builder $query, $shift) {
                return $query->where('shift_name', $shift);
            })
            ->when($filters['status'] ?? null, function (Builder $query, $status) {
                return $query->where('status', '=', $status);
            })
            ->when($filters['trashed'] ?? null, function ($query, $trashed) {
                if ($trashed === 'with') {
                    $query->withTrashed();
                } elseif ($trashed === 'only') {
                    $query->onlyTrashed();
                }
            });
    }

    public function scopeWhereMine(Builder $query, bool $canViewAllData = false) {
        if ($canViewAllData) {
            return $query;
        }

        return $query->whereHasIn('personnelLogs', function ($query) {
            return $query->whereJsonContains('personnel_detail->email', auth()->user()->email);
        });
    }
}
