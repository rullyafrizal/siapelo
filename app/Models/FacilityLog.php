<?php

namespace App\Models;

use App\Traits\UseUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FacilityLog extends Model
{
    use HasFactory, UseUuid;

    protected $guarded = [];
}
