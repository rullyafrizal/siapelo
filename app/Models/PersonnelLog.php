<?php

namespace App\Models;

use App\Traits\UseUuid;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PersonnelLog extends Model
{
    use HasFactory, UseUuid;

    protected $guarded = [];

    public function operationalLog(): BelongsTo
    {
        return $this->belongsTo(OperationalLog::class);
    }

    public function scopeWhereMine(Builder $query)
    {
        return $query->whereJsonContains('personnel_detail->email', auth()->user()->email);
    }

    public function scopeFilters(Builder $query, array $filters = [])
    {
        return $query
            ->when($filters['periodStart'] ?? null, function (Builder $query, $periodStart) {
                return $query->whereHasIn('operationalLog', function ($query) use ($periodStart) {
                    return $query->whereDate('log_date', '>=', $periodStart);
                });
            })
            ->when($filters['periodEnd'] ?? null, function (Builder $query, $periodEnd) {
                return $query->whereHasIn('operationalLog', function ($query) use ($periodEnd) {
                    return $query->whereDate('log_date', '<=', $periodEnd);
                });
            })
            ->when($filters['search'] ?? null, function (Builder $query, $search) {
                return $query->whereRaw("personnel_detail->>'first_name' ILIKE ?", ["%{$search}%"])
                    ->orWhereRaw("personnel_detail->>'last_name' ILIKE ?", ["%{$search}%"]);
            })
            ->when($filters['workShift'] ?? null, function (Builder $query, $workShift) {
                return $query->whereHasIn('operationalLog', function ($query) use ($workShift) {
                    return $query->where('shift_name', '=', $workShift);
                });
            });
    }
}
