<?php

namespace App\Jobs;

use App\Enums\ReviewStatus;
use App\Models\OperationalLog;
use App\Models\ReviewHistory;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class AutoRejectLog implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Scheduler
        DB::transaction(function () {
            $thresholdTime = Carbon::now()->subHours(72);
            $updatedIds = OperationalLog::query()
                ->where('created_at', '<', $thresholdTime)
                ->where(function ($query) {
                    return $query->whereIn('status', [ReviewStatus::IN_PROGRESS, ReviewStatus::NEED_APPROVAL])
                        ->orWhereNull('status');
                })
                ->get()
                ->map(function ($oplog) {
                    return $oplog->id;
                })->toArray();

            OperationalLog::query()
                ->whereIn('id', $updatedIds)
                ->update([
                    'status' => ReviewStatus::REJECTED
                ]);

            $reviewHistories = [];
            foreach ($updatedIds as $updatedId) {
                $reviewHistories[] = [
                    'id' => (string) Str::uuid(),
                    'reviewer_notes' => 'Auto Rejection',
                    'reviewed_by' => json_encode([
                        'id' => 'SYSTEM',
                        'first_name' => 'SYSTEM',
                        'last_name' => '',
                        'email' => 'admin@admin.com'
                    ]),
                    'reviewed_at' => now(),
                    'status' => ReviewStatus::REJECTED,
                    'operational_log_id' => $updatedId,
                ];
            }

            ReviewHistory::query()
                ->insert($reviewHistories);
        });
    }
}
