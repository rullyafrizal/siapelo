<?php

namespace App\Jobs;

use App\Models\OperationalLog;
use App\Models\User;
use App\Notifications\ReviewLogReminderNotification;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class ReviewLogReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $managers;
    private $operationalLog;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(OperationalLog $operationalLog)
    {
        $this->operationalLog = $operationalLog;
        $this->managers = User::query()
            ->whereHasIn('position', function ($query) {
                $query->where('name', '=', 'ATS Operation Manager');
            })
            ->get();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->operationalLog != null) {
            if ($this->operationalLog->last_review_reminder_sent_at == null ||
                now()->diffInHours($this->operationalLog->last_review_reminder_sent_at) > 2) {
                Notification::send($this->managers, new ReviewLogReminderNotification($this->operationalLog));

                $this->operationalLog->update([
                    'last_review_reminder_sent_at' => now()
                ]);
            }
        }
    }
}
