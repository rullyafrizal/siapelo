<?php

namespace App\Enums;

class RoleEnum
{
    const ADMIN = "Admin";
    const STAFF = "Staff";
    const MANAGER = "Manager";
}
