<?php

namespace App\Enums;

class ReviewStatus
{
    const IN_PROGRESS = 'IN_PROGRESS';
    const NEED_APPROVAL = 'NEED_APPROVAL';
    const APPROVED = 'APPROVED';
    const REJECTED = 'REJECTED';
}
