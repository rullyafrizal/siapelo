<?php

namespace App\Notifications;

use App\Enums\DateFormat;
use App\Models\OperationalLog;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReviewLogReminderNotification extends Notification
{
    use Queueable;

    private $operationalLog;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(OperationalLog $operationalLog)
    {
        $this->operationalLog = $operationalLog;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('[Siapelo] Review Reminder Operational Log')
            ->greeting('Kepada Yth. ATS Operation Manager ATC Bandar Lampung')
            ->line('Bapak/Ibu **' . $notifiable->first_name . ' ' . $notifiable->last_name . '**')
            ->line('Dimohon untuk segera melakukan review pada Operational Log pada tanggal '
                . Carbon::parse($this->operationalLog->log_date)
                    ->locale('id')
                    ->format(DateFormat::ONLY_DATE_2)  . ' shift ' . $this->operationalLog->log_shift . ' di bawah ini.')
            ->action('Lihat Detail Operational Log', url(config('url') . '/operational-logs/' . $this->operationalLog->id))
            ->line('Harap segera melakukan review sebelum batas akhir maksimal 72 jam setelah Operational Log dibuat.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
