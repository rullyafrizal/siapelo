<?php

namespace App\Notifications;

use App\Enums\DateFormat;
use App\Enums\ReviewStatus;
use App\Models\OperationalLog;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReviewResultNotification extends Notification
{
    use Queueable;

    private $operationalLog;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(OperationalLog $operationalLog)
    {
        $this->operationalLog = $operationalLog;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mailMessage = (new MailMessage)
            ->subject('[Siapelo] ' . $this->operationalLog->review_status . ' Review Operational Log')
            ->greeting('Kepada Yth. Staff ATC Bandar Lampung')
            ->line('Operational log anda telah direview dengan detail sebagai berikut: ')
            ->line('Tanggal Log: **' . Carbon::parse($this->operationalLog->log_date)
                    ->locale('id')
                    ->format(DateFormat::ONLY_DATE_2) . '**')
            ->line('Shift Log: **' . $this->operationalLog->log_shift . '**')
            ->line('Hasil Review: **' . $this->operationalLog->review_status . '**')
            ->action('Lihat Detail Operational Log', url(config('url') . '/operational-logs/' . $this->operationalLog->id));

        if ($this->operationalLog->review_status == ReviewStatus::APPROVED) {
            return $mailMessage
                ->line('Terimakasih atas perhatiannya.');
        }

        return $mailMessage
            ->line('Mohon untuk segera melakukan revisi dokumen log sebelum batas akhir update log yaitu **72 jam** sejak log dibuat.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
