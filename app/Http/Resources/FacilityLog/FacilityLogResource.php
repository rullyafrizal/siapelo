<?php

namespace App\Http\Resources\FacilityLog;

use App\Enums\DateFormat;
use Illuminate\Http\Resources\Json\JsonResource;

class FacilityLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'time' => to_carbon($this->time)->format(DateFormat::HOUR_MINUTE),
            'name' => $this->name,
            'description' => $this->description,
            'status' => $this->status,
            'created_at' => $this->created_at->format(DateFormat::WITH_TIME),
            'updated_at' => $this->updated_at->format(DateFormat::WITH_TIME),
            'created_by' => json_decode($this->created_by),
            'updated_by' => json_decode($this->updated_by)
        ];
    }
}
