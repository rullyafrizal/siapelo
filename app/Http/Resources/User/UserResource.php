<?php

namespace App\Http\Resources\User;

use App\Enums\DateFormat;
use App\Http\Resources\Position\PositionResource;
use App\Http\Resources\Role\RoleResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'full_name' => $this->first_name . ' ' . $this->last_name,
            'email' => $this->email,
            'roles' => RoleResource::collection($this->whenLoaded('roles')),
            'roles_by_name' => $this->roles->pluck('name'),
            'position' => new PositionResource($this->whenLoaded('position')),
            'created_at' => optional($this->created_at)->format(DateFormat::WITH_TIME),
            'updated_at' => optional($this->updated_at)->format(DateFormat::WITH_TIME),
            'deleted_at' => optional($this->deleted_at)->format(DateFormat::WITH_TIME)
        ];
    }
}
