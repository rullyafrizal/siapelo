<?php

namespace App\Http\Resources\PersonnelLog;

use App\Enums\DateFormat;
use App\Http\Resources\OperationalLog\OperationalLogResource;
use App\Models\OperationalLog;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PersonnelLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => json_decode($this->personnel_detail)->first_name . ' ' . json_decode($this->personnel_detail)->last_name,
            'time' => $this->time ? Carbon::parse($this->time)->format(DateFormat::HOUR_MINUTE) : '',
            'personnel_detail' => json_decode($this->personnel_detail),
            'sign' => $this->sign,
            'created_at' => optional($this->created_at)->format(DateFormat::WITH_TIME),
            'updated_at' => optional($this->updated_at)->format(DateFormat::WITH_TIME),
            'operational_log' => new OperationalLogResource($this->whenLoaded('operationalLog'))
        ];
    }
}
