<?php

namespace App\Http\Resources\Position;

use App\Enums\DateFormat;
use Illuminate\Http\Resources\Json\JsonResource;

class PositionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'created_at' => optional($this->created_at)->format(DateFormat::WITH_TIME),
            'updated_at' => optional($this->updated_at)->format(DateFormat::WITH_TIME),
            'deleted_at' => optional($this->deleted_at)->format(DateFormat::WITH_TIME)
        ];
    }
}
