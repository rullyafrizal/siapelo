<?php

namespace App\Http\Resources\OperationalLog;

use App\Enums\DateFormat;
use App\Enums\ReviewStatus;
use App\Http\Resources\EventLog\EventLogCollection;
use App\Http\Resources\FacilityLog\FacilityLogCollection;
use App\Http\Resources\PersonnelLog\PersonnelLogCollection;
use App\Http\Resources\ReviewHistory\ReviewHistoryCollection;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class OperationalLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'log_date_with_day' => $this->log_date ? Carbon::parse($this->log_date)->format(DateFormat::DATE_WITH_DAY_FULL) : '-',
            'log_date' => $this->log_date,
            'shift_name' => $this->shift_name,
            'shift_start_time' => $this->shift_start_time,
            'shift_end_time' => $this->shift_end_time,
            'shift_name_with_time' => $this->shift_name . " (" . format_time($this->shift_start_time) . "-" . format_time($this->shift_end_time) . ")",
            'review_status' => $this->status,
            'status' => $this->status,
            'created_at' => $this->created_at->format(DateFormat::WITH_TIME),
            'created_by' => json_decode($this->created_by),
            'updated_by' => json_decode($this->updated_by),
            'updated_at' => $this->updated_at->format(DateFormat::WITH_TIME),
            'deleted_at' => optional($this->deleted_at)->format(DateFormat::WITH_TIME),
            'event_logs' => new EventLogCollection($this->whenLoaded('eventLogs')),
            'personnel_logs' => new PersonnelLogCollection($this->whenLoaded('personnelLogs')),
            'review_histories' => new ReviewHistoryCollection($this->whenLoaded('reviewHistories')),
            'facilities' => new FacilityLogCollection($this->whenLoaded('facilityLogs')),
            'is_updatable' => $this->isUpdatable(),
            'ready_to_review' => (bool) $this->ready_to_review,
            'is_reviewable' => $this->isReviewable() && !$this->deleted_at,
            'last_review_reminder_sent_at' => $this->last_review_reminder_sent_at,
            'ready_to_export' => $this->isReadyToExport()
        ];
    }

    public function isReviewable(): bool
    {
        return (bool) $this->ready_to_review && $this->status != ReviewStatus::APPROVED && now()->diffInHours($this->created_at) < 72;
    }

    public function isUpdatable(): bool {
        if (now()->diffInHours($this->created_at) < 72 && $this->status == ReviewStatus::APPROVED) {
            return false;
        } else if (now()->diffInHours($this->created_at) < 72)  {
            return true;
        }

        return false;
    }

    public function isReadyToExport(): bool
    {
        return $this->status == ReviewStatus::APPROVED;
    }
}
