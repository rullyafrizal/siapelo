<?php

namespace App\Http\Resources\ReviewHistory;

use App\Enums\DateFormat;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ReviewHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'reviewed_at' => Carbon::parse($this->reviewed_at)->format(DateFormat::WITH_TIME),
            'reviewed_by' => json_decode($this->reviewed_by),
            'reviewer_notes' => $this->reviewer_notes ?? '-'
        ];
    }
}
