<?php

namespace App\Http\Resources\EventLog;

use App\Enums\DateFormat;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EventLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'time' => to_carbon($this->time)->format(DateFormat::HOUR_MINUTE),
            'specification' => $this->specification,
            'created_at' => $this->created_at->format(DateFormat::WITH_TIME),
            'updated_at' => $this->updated_at->format(DateFormat::WITH_TIME),
            'created_by' => json_decode($this->created_by),
            'updated_by' => json_decode($this->created_by),
            'is_updatable' => $this->isUpdatable()
        ];
    }

    private function isUpdatable() {
        $createdBy = json_decode($this->created_by);
        if ($createdBy->email === auth()->user()->email) {
            return true;
        }

        return false;
    }
}
