<?php

namespace App\Http\Resources\LogShift;

use Illuminate\Http\Resources\Json\ResourceCollection;

class LogShiftCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data' => LogShiftResource::collection($this->collection)
        ];
    }
}
