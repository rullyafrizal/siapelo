<?php

namespace App\Http\Resources\LogShift;

use App\Enums\DateFormat;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class LogShiftResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'start_time' => format_time($this->start_time),
            'end_time' => format_time($this->end_time),
            'name_with_time' => $this->name . " (" . format_time($this->start_time) . "-" . format_time($this->end_time) . ")",
            'created_at' => optional($this->created_at)->format(DateFormat::WITH_TIME),
            'updated_at' => optional($this->updated_at)->format(DateFormat::WITH_TIME),
            'deleted_at' => optional($this->deleted_at)->format(DateFormat::WITH_TIME)
        ];
    }
}
