<?php

namespace App\Http\Controllers;

use App\Http\Requests\LogShift\StoreLogShiftRequest;
use App\Http\Requests\LogShift\UpdateLogShiftRequest;
use App\Http\Resources\LogShift\LogShiftCollection;
use App\Http\Resources\LogShift\LogShiftResource;
use App\Models\LogShift;
use Illuminate\Http\Request;
use Inertia\Inertia;

class LogShiftController extends Controller
{
    public function index(Request $request) {
        $this->authorize('view-log-shifts', LogShift::class);

        $filters = $request->only(['search', 'trashed']);

        return Inertia::render('LogShifts/Index', [
            'filters' => $filters,
            'logShifts' => new LogShiftCollection(
                LogShift::filters($filters)
                    ->orderBy('name')
                    ->paginate()
            )
        ]);
    }

    /**
     * @return \Inertia\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create-log-shift', LogShift::class);

        return Inertia::render('LogShifts/Create');
    }

    public function store(StoreLogShiftRequest $request)
    {
        $this->authorize('create-log-shift', LogShift::class);

        LogShift::query()
            ->create([
                'name' => strtoupper($request->get('name')),
                'start_time' => $request->get('start_time'),
                'end_time' => $request->get('end_time')
            ]);

        return redirect()
            ->route('log-shifts.index')
            ->with('success', 'Log Shift Successfully Added');
    }

    public function edit(LogShift $logShift)
    {
        $this->authorize('edit-log-shift', LogShift::class);

        return Inertia::render('LogShifts/Edit', [
            'logShift' => new LogShiftResource($logShift)
        ]);
    }

    public function update(LogShift $logShift, UpdateLogShiftRequest $request)
    {
        $this->authorize('edit-log-shift', LogShift::class);

        $logShift->update([
            'name' => strtoupper($request->get('name')),
            'start_time' => $request->get('start_time'),
            'end_time' => $request->get('end_time')
        ]);

        return redirect()
            ->route('log-shifts.index')
            ->with('success', 'Log Shift Successfully Updated');
    }

    public function destroy(LogShift $logShift)
    {
        $this->authorize('delete-log-shift', LogShift::class);

        $logShift->delete();

        return redirect()
            ->route('log-shifts.index')
            ->with('success', 'Log Shift Successfully Deleted');
    }

    public function restore(LogShift $logShift)
    {
        $this->authorize('restore-log-shift', LogShift::class);

        $logShift->restore();

        return redirect()
            ->route('log-shifts.index')
            ->with('success', 'Log Shift Successfully Restored');
    }
}
