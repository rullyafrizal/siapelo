<?php

namespace App\Http\Controllers;

use App\Enums\DateFormat;
use App\Http\Resources\LogShift\LogShiftCollection;
use App\Http\Resources\LogShift\LogShiftResource;
use App\Http\Resources\PersonnelLog\PersonnelLogCollection;
use App\Models\EventLog;
use App\Models\LogShift;
use App\Models\OperationalLog;
use App\Models\PersonnelLog;
use App\Services\OplogService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;

class PersonnelLogController extends Controller
{

    private OplogService $oplogService;

    public function __construct(OplogService $oplogService)
    {
        $this->oplogService = $oplogService;
    }

    public function index(Request $request)
    {
        $this->authorize('view-personnel-logs', PersonnelLog::class);
        $filters = $request->only(['periodStart', 'periodEnd', 'search', 'workShift']);

        $personnelLogs = new PersonnelLogCollection(
            PersonnelLog::query()
                ->with(['operationalLog'])
                ->withCount(['operationalLog'])
                ->has('operationalLog', '>', 0)
                ->whereMine()
                ->orderByDesc('created_at')
                ->filters($filters)
                ->paginate(5)
        );

        if (auth()->user()->can('view-all-personnel-logs')) {
            $personnelLogs = new PersonnelLogCollection(
                PersonnelLog::query()
                    ->with(['operationalLog'])
                    ->withCount(['operationalLog'])
                    ->has('operationalLog', '>', 0)
                    ->orderByDesc('created_at')
                    ->filters($filters)
                    ->paginate(5)
            );
        }


        return Inertia::render('PersonnelLogs/Index', [
            'filters' => $filters,
            'personnelLogs' => $personnelLogs,
            'showClockInButton' => $this->isShowClockInButton(),
            'logShifts' => new LogShiftCollection(LogShift::query()->get())
        ]);
    }

    private function isShowClockInButton(): bool
    {
        $currentShift = OperationalLog::query()
            ->where('log_date', '=', Carbon::now()->format(DateFormat::ONLY_DATE))
            ->where('shift_start_time', '<=', nowInHisFormat())
            ->where('shift_end_time', '>=', nowInHisFormat())
            ->whereMine()
            ->count();

        return !$currentShift && !auth()->user()->can('full-access')
            && auth()->user()->can('clock-in-personnel-logs');
    }

    /**
     * Clock in personnel
     */
    public function create()
    {
        $this->authorize('clock-in-personnel-logs', PersonnelLog::class);
        $shift = new LogShiftResource(
            LogShift::query()
                ->where('start_time', '<=', nowInHisFormat())
                ->where('end_time', '>=', nowInHisFormat())
                ->first()
        );
        $oplog = OperationalLog::query()
            ->whereDate('log_date', '=', Carbon::now()->format(DateFormat::ONLY_DATE))
            ->where('shift_name', '=', $shift->name)
            ->first();

        if ($oplog) {
            PersonnelLog::query()
                ->create([
                    'id' => (string) Str::uuid(),
                    'time' => nowInHisFormat(),
                    'personnel_detail' => json_encode([
                        'id' => auth()->id(),
                        'first_name' => auth()->user()->first_name,
                        'last_name' => auth()->user()->last_name,
                        'email' => auth()->user()->email
                    ]),
                    'sign' => true,
                    'operational_log_id' => $oplog->id,
                    'created_at' => now(),
                    'updated_at' => now()
                ]);

            EventLog::query()
                ->create([
                    'time' => nowInHisFormat(),
                    'specification' => 'Personnel ' . auth()->user()->first_name . ' ' . auth()->user()->last_name . ' clock in',
                    'operational_log_id' => $oplog->id,
                    'created_by' => json_encode([
                        'id' => 'SYSTEM',
                        'first_name' => 'SYSTEM',
                        'last_name' => 'SYSTEM',
                        'email' => 'SYSTEM'
                    ]),
                    'updated_by' => json_encode([
                        'id' => 'SYSTEM',
                        'first_name' => 'SYSTEM',
                        'last_name' => 'SYSTEM',
                        'email' => 'SYSTEM'
                    ])
                ]);
        } else {
            $shift = $this->oplogService->createOperationalLog();
        }

        return redirect()
            ->route('personnel-logs.index')
            ->with('success', 'Successfully clock in for shift ' . $shift->name . '  at ' . Carbon::now()->format(DateFormat::ONLY_DATE));
    }
}
