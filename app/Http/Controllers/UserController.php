<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\Position\PositionCollection;
use App\Http\Resources\Role\RoleCollection;
use App\Http\Resources\User\UserCollection;
use App\Http\Resources\User\UserResource;
use App\Models\Position;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index(\Illuminate\Http\Request $request)
    {
        $this->authorize('view-users', User::class);

        $filters = $request->only(['search', 'role', 'trashed']);

        return Inertia::render('Users/Index', [
            'filters' => $filters,
            'users' => new UserCollection(
                User::query()
                    ->with(['roles', 'position'])
                    ->orderByDesc('created_at')
                    ->filters($filters)
                    ->paginate()
            ),
        ]);
    }

    public function create()
    {
        $this->authorize('create-user', User::class);

        $admin = User::admin()->first();
        $manager = User::manager()->first();
        $roles = Role::query()->get(['id', 'name']);
        if ($admin && $manager) {
            $roles = Role::query()
                ->where('name', '!=', 'Admin')
                ->where('name', '!=', 'Manager')
                ->get(['id', 'name']);
        } else if ($admin && !$manager) {
            $roles = Role::query()
                ->where('name', '!=', 'Manager')
                ->get(['id', 'name']);
        } else if (!$admin && $manager) {
            $roles = Role::query()
                ->where('name', '!=', 'Admin')
                ->get(['id', 'name']);
        }

        return Inertia::render('Users/Create', [
            'roles' => new RoleCollection($roles),
            'positions' => new PositionCollection(Position::query()->get(['id', 'name']))
        ]);
    }

    public function store(StoreUserRequest $request)
    {
        $this->authorize('create-user', User::class);

        DB::transaction(function () use ($request) {
            $user = User::query()
                ->create([
                    'first_name' => $request->get('first_name'),
                    'last_name' => $request->get('last_name'),
                    'password' => bcrypt($request->get('password')),
                    'email' => $request->get('email'),
                    'position_id' => $request->get('position'),
                ]);

            $user->syncRoles([$request->get('role')]);
        });

        return Redirect::route('users.index')
            ->with('success', 'User has been created');
    }

    public function edit(User $user)
    {
        $this->authorize('edit-user', User::class);

        return Inertia::render('Users/Edit', [
            'user' => new UserResource($user->load(['roles', 'position'])),
            'roles' => new RoleCollection(Role::query()->get(['id', 'name'])),
            'positions' => new PositionCollection(Position::query()->get(['id', 'name'])),
            'isAbleToChangePassword' => auth()->id() == $user->id
        ]);
    }

    public function update(User $user, UpdateUserRequest $request)
    {
        $this->authorize('edit-user', User::class);

        DB::transaction(function () use ($user, $request) {
            $user->update([
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'email' => $request->get('email'),
                'password' => $request->get('password') == '' ?
                    $user->password : bcrypt($request->get('password')),
                'position_id' => $request->get('position')
            ]);

            $user->syncRoles([$request->get('role')]);
        });

        return Redirect::route('users.edit', $user->id)
            ->with('success', 'User successfully edited');
    }

    public function destroy(User $user)
    {
        $this->authorize('delete-user', User::class);

        DB::transaction(function () use ($user) {
            $user->delete();
        });

        return Redirect::route('users.index')
            ->with('success', 'User successfully deleted');
    }

    public function restore(User $user)
    {
        $user->restore();

        return Redirect::route('users.index')
            ->with('success', 'User successfully restored');
    }
}
