<?php

namespace App\Http\Controllers;

use App\Http\Requests\Facility\StoreFacilityRequest;
use App\Http\Requests\Facility\UpdateFacilityRequest;
use App\Http\Resources\Facility\FacilityCollection;
use App\Http\Resources\Facility\FacilityResource;
use App\Models\Facility;
use Illuminate\Http\Request;
use Inertia\Inertia;

class FacilityController extends Controller
{
    public function index(Request $request) {
        $this->authorize('view-facilities', Facility::class);

        $filters = $request->only(['search', 'trashed']);

        return Inertia::render('Facilities/Index', [
            'filters' => $filters,
            'facilities' => new FacilityCollection(
                Facility::filters($filters)
                    ->orderByDesc('created_at')
                    ->paginate()
            )
        ]);
    }

    /**
     * @return \Inertia\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create-facility', Facility::class);

        return Inertia::render('Facilities/Create');
    }

    public function store(StoreFacilityRequest $request)
    {
        $this->authorize('create-facility', Facility::class);

        Facility::query()
            ->create([
                'name' => $request->get('name')
            ]);

        return redirect()
            ->route('facilities.index')
            ->with('success', 'Facility Successfully Added');
    }

    public function edit(Facility $facility)
    {
        $this->authorize('edit-facility', Facility::class);

        return Inertia::render('Facilities/Edit', [
            'facility' => new FacilityResource($facility)
        ]);
    }

    public function update(Facility $facility, UpdateFacilityRequest $request)
    {
        $this->authorize('edit-facility', Facility::class);

        $facility->update([
            'name' => $request->get('name')
        ]);

        return redirect()
            ->route('facilities.index')
            ->with('success', 'Facility Successfully Updated');
    }

    public function destroy(Facility $facility)
    {
        $this->authorize('delete-facility', Facility::class);

        $facility->delete();

        return redirect()
            ->route('facilities.index')
            ->with('success', 'Facility Successfully Deleted');
    }

    public function restore(Facility $facility)
    {
        $this->authorize('restore-facility', Facility::class);

        $facility->restore();

        return redirect()
            ->route('facilities.index')
            ->with('success', 'Facility Successfully Restored');
    }
}
