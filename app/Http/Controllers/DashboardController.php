<?php

namespace App\Http\Controllers;

use App\Enums\ReviewStatus;
use App\Models\OperationalLog;
use App\Models\Position;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $filters = $request->only(['year']);

        $year = now()->format("Y");

        if (array_key_exists('year', $filters)) {
            $year = (int) $filters['year'];
        }

        $positions = Position::query()->get()
            ->map(function (Position $position) {
                return [
                    'usersCount' => $position->users()->count(),
                    'name' => $position->name,
                    'id' => $position->id
                ];
            });

        $canViewAllData = auth()->user()->hasAnyRole(['Admin', 'Manager']);

        return Inertia::render('Dashboard/Index', [
            'filters' => $filters,
            'oplogCountAllTime' => OperationalLog::query()
                ->whereMine($canViewAllData)
                ->count(),
            'oplogCountApprovedAllTime' => OperationalLog::query()
                ->where('status', ReviewStatus::APPROVED)
                ->whereMine($canViewAllData)
                ->count(),
            'oplogCountRejectedAllTime' => OperationalLog::query()
                ->where('status', ReviewStatus::REJECTED)
                ->whereMine($canViewAllData)
                ->count(),
            'oplogCountNeedApprovalAllTime' => OperationalLog::query()
                ->where('status', ReviewStatus::NEED_APPROVAL)
                ->whereMine($canViewAllData)
                ->count(),
            'oplogCountInProgressAllTime' => OperationalLog::query()
                ->where('status', ReviewStatus::IN_PROGRESS)
                ->whereMine($canViewAllData)
                ->count(),
            'userCount' => auth()->user()->can('view-user-count-dashboard') ? User::query()->count() : 0,
            'positions' => auth()->user()->can('view-user-count-dashboard') ? $positions : [],
        ]);
    }
}
