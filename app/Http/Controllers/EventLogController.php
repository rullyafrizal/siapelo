<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventLog\UpdateEventLogRequest;
use App\Http\Resources\LogShift\LogShiftCollection;
use App\Http\Resources\OperationalLog\OperationalLogCollection;
use App\Http\Resources\OperationalLog\OperationalLogResource;
use App\Models\EventLog;
use App\Models\LogShift;
use App\Models\OperationalLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Inertia\Inertia;

class EventLogController extends Controller
{
    public function index(Request $request) {
        $this->authorize('view-event-logs', EventLog::class);

        $filters = $request->only(['periodStart', 'periodEnd', 'work_shift']);

        return Inertia::render('EventLogs/Index', [
            'filters' => $filters,
            'operationalLogs' => new OperationalLogCollection(
                OperationalLog::query()
                    ->whereMine(auth()->user()->can('view-all-event-logs'))
                    ->with(['eventLogs' => function ($query) {
                        return $query
                            ->whereJsonDoesntContain('created_by->id', 'SYSTEM')
                            ->orderBy('created_at');
                    }])
                    ->orderByDesc('log_date')
                    ->filters($filters)
                    ->paginate(10),
            ),
            'logShifts' => new LogShiftCollection(LogShift::query()->get())
        ]);
    }

    public function edit(OperationalLog $operationalLog)
    {
        $this->authorize('edit-event-logs', EventLog::class);
        $checkOperationalLog = OperationalLog::query()
            ->where('id', '=', $operationalLog->id)
            ->whereMine(auth()->user()->can('view-all-event-logs'))
            ->count();

        if (!$checkOperationalLog) {
            return redirect()
                ->route('facility-logs.index')
                ->with('error', 'You have no access to edit event logs for this shift');
        }

        return Inertia::render('EventLogs/Edit', [
            'operationalLog' => new OperationalLogResource(
                $operationalLog->load(['eventLogs' => function ($query) {
                    return $query
                        ->whereJsonDoesntContain('created_by->id', 'SYSTEM')
                        ->orderBy('created_at');
                }])
            )
        ]);
    }

    public function update(OperationalLog $operationalLog, UpdateEventLogRequest $request)
    {
        $this->authorize('edit-event-logs', EventLog::class);

        DB::transaction(function() use ($operationalLog, $request) {
            $eventLogs = $request->get('event_logs');
            $eventLogsInsert = [];
            $eventLogsUpsert = [];
            $auth = json_encode([
                'id' => auth()->id(),
                'first_name' => auth()->user()->first_name,
                'last_name' => auth()->user()->last_name,
                'email' => auth()->user()->email
            ]);

            foreach ($eventLogs as $eventLog) {
                if ($eventLog['id']) {
                    $eventLogsUpsert[] = [
                        'id' => $eventLog['id'],
                        'time' => $eventLog['time'],
                        'specification' => $eventLog['specification'],
                        'operational_log_id' => $request->get('operational_log'),
                        'updated_at' => now(),
                        'updated_by' => $auth
                    ];
                } else {
                    $eventLogsInsert[] = [
                        'id' => (string) Str::uuid(),
                        'time' => $eventLog['time'],
                        'specification' => $eventLog['specification'],
                        'operational_log_id' => $request->get('operational_log'),
                        'created_at' => now(),
                        'updated_at' => now(),
                        'created_by' => $auth,
                        'updated_by' => $auth
                    ];
                }
            }

            EventLog::query()->insert($eventLogsInsert);
            collect($eventLogsUpsert)->each(function($ev) use ($auth) {
                EventLog::query()
                    ->where('id', '=', $ev['id'])
                    ->where('time', '=', $ev['time'])
                    ->where('specification', '=', $ev['specification'])
                    ->firstOr(function () use ($ev, $auth) {
                        EventLog::query()
                            ->where('id', '=', $ev['id'])
                            ->update([
                                'time' => $ev['time'],
                                'specification' => $ev['specification'],
                                'updated_at' => now(),
                                'updated_by' => $auth
                            ]);
                    });
            });
            OperationalLog::query()
                ->where('id', '=', $request->get('operational_log'))
                ->update([
                    'updated_by' => $auth,
                ]);
        });

        return redirect()
            ->route('event-logs.index')
            ->with('success', 'Event logs successfully updated');
    }
}
