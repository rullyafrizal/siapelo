<?php

namespace App\Http\Controllers;

use App\Enums\DateFormat;
use App\Enums\ReviewStatus;
use App\Exports\FacilityLogExport;
use App\Http\Requests\FacilityLog\StoreFacilityLogRequest;
use App\Http\Requests\FacilityLog\UpdateFacilityLogRequest;
use App\Http\Resources\Facility\FacilityCollection;
use App\Http\Resources\LogShift\LogShiftCollection;
use App\Http\Resources\LogShift\LogShiftResource;
use App\Http\Resources\OperationalLog\OperationalLogCollection;
use App\Http\Resources\OperationalLog\OperationalLogResource;
use App\Models\EventLog;
use App\Models\Facility;
use App\Models\FacilityLog;
use App\Models\LogShift;
use App\Models\OperationalLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class FacilityLogController extends Controller
{
    public function index(Request $request) {
        $this->authorize('view-facilities-logs', FacilityLog::class);

        $filters = $request->only(['periodStart', 'periodEnd', 'work_shift']);
        $shift = new LogShiftResource(
            LogShift::query()
                ->where('start_time', '<=', nowInHisFormat())
                ->where('end_time', '>=', nowInHisFormat())
                ->first()
        );
        $oplogCount = OperationalLog::query()
            ->whereDate('log_date', '=', Carbon::now()->format(DateFormat::ONLY_DATE))
            ->where('shift_name', '=', $shift->name)
            ->whereMine(auth()->user()->can('view-all-facilities-logs'))
            ->with(['facilityLogs' => function ($query) {
                return $query->orderBy('name');
            }])
            ->withCount(['facilityLogs'])
            ->has('facilityLogs', '=', 0)
            ->count();
        return Inertia::render('FacilityLogs/Index', [
            'filters' => $filters,
            'operationalLogs' => new OperationalLogCollection(
                OperationalLog::query()
                    ->with(['facilityLogs'])
                    ->withCount(['facilityLogs'])
                    ->has('facilityLogs', '>', 0)
                    ->whereMine(auth()->user()->can('view-all-facilities-logs'))
                    ->orderByDesc('log_date')
                    ->filters($filters)
                    ->paginate(5)
            ),
            'showCreateButton' => (bool) $oplogCount,
            'logShifts' => new LogShiftCollection(LogShift::query()->get())
        ]);
    }

    public function create()
    {
        $this->authorize('create-facilities-logs', FacilityLog::class);

        $shift = new LogShiftResource(
            LogShift::query()
                ->where('start_time', '<=', nowInHisFormat())
                ->where('end_time', '>=', nowInHisFormat())
                ->first()
        );
        $oplog = OperationalLog::query()
            ->whereMine(auth()->user()->can('view-all-facilities-logs'))
            ->whereDate('log_date', '=', Carbon::now()->format(DateFormat::ONLY_DATE))
            ->where('shift_name', '=', $shift->name)
            ->first();

        if (!$oplog) {
            return redirect()
                ->route('facility-logs.index')
                ->with('error', 'You have no access to create facility logs for this shift');
        }

        $facilities = new FacilityCollection(
            Facility::query()
                ->orderBy('name')
                ->get()
        );
        $facilitiesForFe = [];

        foreach ($facilities as $facility) {
            $facilitiesForFe[] = [
                'description' => '',
                'name' => $facility->name,
                'id' => $facility->id,
                'status' => 'FUNCTIONAL',
            ];
        }

        return Inertia::render('FacilityLogs/Create', [
            'operationalLog' => new OperationalLogResource($oplog),
            'facilities' => $facilitiesForFe
        ]);
    }

    public function store(StoreFacilityLogRequest $request, OperationalLog $operationalLog)
    {
        $this->authorize('create-facilities-logs', FacilityLog::class);

        DB::transaction(function () use ($request, $operationalLog) {
            $facilityInsert = [];
            $auth = json_encode([
                'id' => auth()->id(),
                'first_name' => auth()->user()->first_name,
                'last_name' => auth()->user()->last_name,
                'email' => auth()->user()->email
            ]);
            foreach ($request->get('facilities') as $facility) {
                $facilityInsert[] = [
                    'id' => (string) Str::uuid(),
                    'name' => $facility['name'],
                    'time' => nowInHisFormat(),
                    'description' => $facility['description'],
                    'created_by' => $auth,
                    'updated_by' => $auth,
                    'status' => $facility['status'],
                    'operational_log_id' => $operationalLog->id,
                    'created_at' => now(),
                    'updated_at' => now()
                ];
            }

            FacilityLog::query()
                ->insert($facilityInsert);

            $operationalLog->update([
                'updated_by' => json_encode([
                    'id' => auth()->id(),
                    'first_name' => auth()->user()->first_name,
                    'last_name' => auth()->user()->last_name,
                    'email' => auth()->user()->email
                ]),
            ]);

            EventLog::query()
                ->create([
                    'time' => nowInHisFormat(),
                    'specification' => 'Facility log has been created by ' . auth()->user()->first_name . ' ' . auth()->user()->last_name,
                    'operational_log_id' => $operationalLog->id,
                    'created_by' => json_encode([
                        'id' => 'SYSTEM',
                        'first_name' => 'SYSTEM',
                        'last_name' => 'SYSTEM',
                        'email' => 'SYSTEM'
                    ]),
                    'updated_by' => json_encode([
                        'id' => 'SYSTEM',
                        'first_name' => 'SYSTEM',
                        'last_name' => 'SYSTEM',
                        'email' => 'SYSTEM'
                    ])
                ]);
        });

        return redirect()
            ->route('facility-logs.index')
            ->with('success', 'Facility logs successfully added');
    }

    public function edit(OperationalLog $operationalLog) {
        $this->authorize('edit-facilities-logs', FacilityLog::class);

        $checkOperationalLog = OperationalLog::query()
            ->where('id', '=', $operationalLog->id)
            ->whereMine(auth()->user()->can('view-all-facilities-logs'))
            ->count();

        if (!$checkOperationalLog) {
            return redirect()
                ->route('facility-logs.index')
                ->with('error', 'You have no access to edit facility logs for this shift');
        }

        $facilitiesForFe = [];
        $facilities = $operationalLog->facilityLogs()
            ->orderBy('name')
            ->get();

        foreach ($facilities as $facility) {
            $facilitiesForFe[] = [
                'description' => $facility->description,
                'name' => $facility->name,
                'status' => $facility->status,
                'id' => $facility->id,
                'updated_by' => json_decode($facility->updated_by),
                'created_by' => json_decode($facility->created_by),
                'created_at' => to_carbon($facility->created_at)->format(DateFormat::WITH_TIME),
                'updated_at' => to_carbon($facility->updated_at)->format(DateFormat::WITH_TIME)
            ];
        }

        return Inertia::render('FacilityLogs/Edit', [
            'operationalLog' => new OperationalLogResource(
                $operationalLog
            ),
            'facilities' => $facilitiesForFe
        ]);
    }

    public function update(UpdateFacilityLogRequest $request, OperationalLog $operationalLog)
    {
        $this->authorize('edit-facilities-logs', FacilityLog::class);

        DB::transaction(function () use ($request, $operationalLog) {
            $auth = json_encode([
                'id' => auth()->id(),
                'first_name' => auth()->user()->first_name,
                'last_name' => auth()->user()->last_name,
                'email' => auth()->user()->email
            ]);
            foreach ($request->get('facilities') as $facility) {
                FacilityLog::query()
                    ->where('id', '=', $facility['id'])
                    ->where('description', '=', $facility['description'])
                    ->where('status', '=', $facility['status'])
                    ->firstOr(function () use ($facility, $auth, $operationalLog) {
                        $initialState = FacilityLog::query()
                            ->where('id', '=', $facility['id'])
                            ->first();

                        FacilityLog::query()
                            ->where('id', '=', $facility['id'])
                            ->update([
                                'description' => $facility['description'],
                                'name' => $facility['name'],
                                'status' => $facility['status'],
                                'id' => $facility['id'],
                                'updated_by' => $auth,
                                'updated_at' => now()
                            ]);

                        $initialStateWording = ' from (status=' . $initialState->status . ',desc=' . $initialState->description . ')';
                        $newStateWording = ' to (status=' . $facility['status'] . ',desc=' . $facility['description'] . ')';
                        EventLog::query()
                            ->create([
                                'time' => nowInHisFormat(),
                                'specification' => 'Facility ' . $facility['name'] .  ' log has been updated by ' .
                                    auth()->user()->first_name . ' ' . auth()->user()->last_name . $initialStateWording . $newStateWording,
                                'operational_log_id' => $operationalLog->id,
                                'created_by' => json_encode([
                                    'id' => 'SYSTEM',
                                    'first_name' => 'SYSTEM',
                                    'last_name' => 'SYSTEM',
                                    'email' => 'SYSTEM'
                                ]),
                                'updated_by' => json_encode([
                                    'id' => 'SYSTEM',
                                    'first_name' => 'SYSTEM',
                                    'last_name' => 'SYSTEM',
                                    'email' => 'SYSTEM'
                                ])
                            ]);
                    });
            }

            $operationalLog->update([
                'updated_by' => json_encode([
                    'id' => auth()->id(),
                    'first_name' => auth()->user()->first_name,
                    'last_name' => auth()->user()->last_name,
                    'email' => auth()->user()->email
                ]),
            ]);
        });

        return redirect()
            ->route('facility-logs.index')
            ->with('success', 'Facility logs successfully added');
    }

    public function exportXls(Request $request): \Illuminate\Http\RedirectResponse | BinaryFileResponse
    {
        $this->authorize('export-recap-facilities-logs', FacilityLog::class);

        $filters = $request->only(['periodStart', 'periodEnd', 'work_shift']);

        $headerFacilities = Facility::query()
            ->orderBy('name')
            ->get()
            ->map(function ($facility) {
                return $facility->name;
            })
            ->toArray();

        $operationalLogs = OperationalLog::query()
            ->where('status', '=', ReviewStatus::APPROVED)
            ->with(['facilityLogs' => function ($query) {
                return $query->orderBy('name');
            }])
            ->withCount(['facilityLogs'])
            ->has('facilityLogs', '>', 0)
            ->whereMine(auth()->user()->can('view-all-facilities-logs'))
            ->orderByDesc('log_date')
            ->filters($filters)
            ->get()
            ->map(function ($oplog) use ($headerFacilities) {
                $facilityLogs = $oplog->facilityLogs
                    ->map(function ($facilityLog) {
                        return [
                            'name' => $facilityLog->name,
                            'status' => $facilityLog->status,
                            'description' => $facilityLog->description ?? '-',
                        ];
                    })
                    ->toArray();

                $secondNames = array_column($facilityLogs, 'name');
                $facilityLogs = array_filter($facilityLogs, function ($item) use ($headerFacilities) {
                    return in_array($item['name'], $headerFacilities);
                });
                foreach ($headerFacilities as $value) {
                    if (!in_array($value, $secondNames)) {
                        $missingValueIndex = array_search($value, $headerFacilities);
                        array_splice($facilityLogs, $missingValueIndex, 0, [['name' => $value, 'status' => '-', 'description' => '-']]);
                    }
                }

                return [
                    'logDate' => $oplog->log_date,
                    'logShift' => $oplog->shift_name . ' (' . to_carbon($oplog->shift_start_time)->format(DateFormat::HOUR_MINUTE)
                        . '-' . to_carbon($oplog->shift_end_time)->format(DateFormat::HOUR_MINUTE)  . ')',
                    'facilityLogs' => $facilityLogs
                ];
            })
            ->toArray();

        if (!count($operationalLogs)) {
            return redirect()
                ->route('facility-logs.index', $filters)
                ->with('error', 'No data to be exported');
        }

        $data = [
            'headerFacilities' => $headerFacilities,
            'operationalLogs' => $operationalLogs
        ];

        return Excel::download(new FacilityLogExport($data), "facility_logs_rekap.xlsx");
    }
}
