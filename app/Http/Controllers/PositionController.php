<?php

namespace App\Http\Controllers;

use App\Http\Requests\Position\StorePositionRequest;
use App\Http\Requests\Position\UpdatePositionRequest;
use App\Http\Resources\Position\PositionCollection;
use App\Http\Resources\Position\PositionResource;
use App\Models\Position;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PositionController extends Controller
{
    public function index(Request $request) {
        $this->authorize('view-positions', Position::class);

        $filters = $request->only(['search', 'trashed']);

        return Inertia::render('Positions/Index', [
            'filters' => $filters,
            'positions' => new PositionCollection(
                Position::filters($filters)
                    ->orderBy('name')
                    ->paginate()
            )
        ]);
    }

    /**
     * @return \Inertia\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create-position', Position::class);

        return Inertia::render('Positions/Create');
    }

    public function store(StorePositionRequest $request)
    {
        $this->authorize('create-position', Position::class);

        Position::query()
            ->create([
                'name' => $request->get('name')
            ]);

        return redirect()
            ->route('positions.index')
            ->with('success', 'Position Successfully Added');
    }

    public function edit(Position $position)
    {
        $this->authorize('edit-position', Position::class);

        return Inertia::render('Positions/Edit', [
            'position' => new PositionResource($position)
        ]);
    }

    public function update(Position $position, UpdatePositionRequest $request)
    {
        $this->authorize('edit-position', Position::class);

        $position->update([
            'name' => $request->get('name')
        ]);

        return redirect()
            ->route('positions.index')
            ->with('success', 'Position Successfully Updated');
    }

    public function destroy(Position $position)
    {
        $this->authorize('delete-position', Position::class);

        $position->delete();

        return redirect()
            ->route('positions.index')
            ->with('success', 'Position Successfully Deleted');
    }

    public function restore(Position $position)
    {
        $this->authorize('restore-position', Position::class);

        $position->restore();

        return redirect()
            ->route('positions.index')
            ->with('success', 'Position Successfully Restored');
    }
}
