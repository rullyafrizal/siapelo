<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\StoreRoleRequest;
use App\Http\Requests\Role\UpdateRoleRequest;
use App\Http\Resources\Role\RoleCollection;
use App\Http\Resources\Role\RoleResource;
use App\Services\RoleService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view-roles', Role::class);

        return Inertia::render('Roles/Index', [
            'filters' => $request->only('search'),
            'roles' => new RoleCollection(
                Role::with('permissions')
                    ->when($request->search, function (Builder $query) use ($request) {
                        return $query->where('name', 'ilike', "%{$request->search}%");
                    })
                    ->orderByDesc('id')
                    ->paginate()
            )
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(RoleService $roleService)
    {
        $this->authorize('create-role', Role::class);

        return Inertia::render('Roles/Create', $roleService->getPermissions());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRoleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function store(StoreRoleRequest $request)
    {
        DB::transaction(function () use ($request) {
            $role = Role::create($request->only('name'));

            $role->syncPermissions($request->permissions);
        });

        return redirect()
            ->route('roles.index')
            ->with('success', 'Roles has been created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Role $role
     * @return \Inertia\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Request $request, Role $role, RoleService $roleService)
    {
        $this->authorize('edit-role', Role::class);

        $role = ['role' => new RoleResource($role)];
        $list_permissions = array_merge($role, $roleService->getPermissions());

        return Inertia::render('Roles/Edit', $list_permissions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRoleRequest $request
     * @param Role $role
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        DB::transaction(function () use ($request, $role) {
            $role->update($request->only('name', 'guard_name'));

            $role->syncPermissions($request->permissions);
        });

        return redirect()
            ->route('roles.index')
            ->with('success', 'Roles has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Role $role
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     * @throws \Throwable
     */
    public function destroy(Role $role)
    {
        $this->authorize('delete-role', Role::class);

        DB::transaction(function () use ($role) {
            $role->permissions()->detach();

            $role->delete();
        });

        return redirect()
            ->route('roles.index')
            ->with('success', 'Roles has been updated.');
    }
}
