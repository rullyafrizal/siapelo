<?php

namespace App\Http\Controllers;

use App\Enums\DateFormat;
use App\Enums\ReviewStatus;
use App\Enums\RoleEnum;
use App\Http\Requests\OperationalLog\ReviewOperationalLogRequest;
use App\Http\Resources\LogShift\LogShiftCollection;
use App\Http\Resources\LogShift\LogShiftResource;
use App\Http\Resources\OperationalLog\OperationalLogCollection;
use App\Http\Resources\OperationalLog\OperationalLogResource;
use App\Models\LogShift;
use App\Models\OperationalLog;
use App\Models\PersonnelLog;
use App\Models\User;
use App\Services\OplogService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Inertia\Inertia;

class OperationalLogController extends Controller
{
    private OplogService $oplogService;

    public function __construct(OplogService $oplogService)
    {
        $this->oplogService = $oplogService;
    }

    public function index(Request $request)
    {
        $this->authorize('view-operational-logs', OperationalLog::class);
        $filters = $request->only(['work_shift', 'search', 'periodStart', 'periodEnd', 'status', 'trashed']);
        $workShifts = new LogShiftCollection(LogShift::query()->get());

        $shift = new LogShiftResource(
            LogShift::query()
                ->where('start_time', '<=', nowInHisFormat())
                ->where('end_time', '>=', nowInHisFormat())
                ->first()
        );
        $duplicateCount = OperationalLog::query()
            ->whereDate('log_date', '=', Carbon::now()->format(DateFormat::ONLY_DATE))
            ->where('shift_name', '=', $shift->name)
            ->count();

        return Inertia::render('OperationalLogs/Index', [
            'operationalLogs' => new OperationalLogCollection(
                OperationalLog::query()
                    ->whereMine(auth()->user()->can('view-all-operational-logs'))
                    ->orderByDesc('created_at')
                    ->filters($filters)
                    ->paginate(5)
            ),
            'filters' => $filters,
            'workShifts' => $workShifts,
            'isShowCreateButton' => !($duplicateCount)
        ]);
    }

    public function create()
    {
        $this->authorize('create-operational-logs', OperationalLog::class);
        $shift = $this->oplogService->createOperationalLog();

        return redirect()
            ->route('operational-logs.index')
            ->with('success', 'Operational log for shift ' . $shift->name . " (" . format_time($shift->start_time) . "-" . format_time($shift->end_time) . ")" . ' has been created');
    }

    public function destroy(OperationalLog $operationalLog)
    {
        $this->authorize('delete-operational-logs', OperationalLog::class);

        $admin = User::admin()->first();
        if (auth()->id() != $admin->id) {
            return redirect()
                ->route('operational-logs.index')
                ->with('error', 'Operational Log can\'t be deleted. You do not have permission to edit this log.');
        }

        DB::transaction(function () use ($operationalLog) {
            $operationalLog->update([
                'deleted_by' => json_encode([
                    'id' => auth()->id(),
                    'first_name' => auth()->user()->first_name,
                    'last_name' => auth()->user()->last_name,
                    'email' => auth()->user()->email
                ])
            ]);
            $operationalLog->delete();
        });

        return redirect()
            ->route('operational-logs.index')
            ->with('success', 'Operational log successfully deleted');
    }

    public function restore(OperationalLog $operationalLog) {
        $this->authorize('restore-operational-logs', OperationalLog::class);

        $admin = User::admin()->first();
        if (auth()->id() != $admin->id) {
            return redirect()
                ->route('operational-logs.index')
                ->with('error', 'Operational Log can\'t be deleted. You do not have permission to edit this log.');
        }

        DB::transaction(function () use ($operationalLog) {
            $operationalLog->update([
                'deleted_by' => null
            ]);
            $operationalLog->restore();
        });

        return redirect()
            ->route('operational-logs.index')
            ->with('success', 'Operational log successfully restored');
    }

    public function show(OperationalLog $operationalLog)
    {
        $this->authorize('show-operational-logs', OperationalLog::class);

        return Inertia::render('OperationalLogs/Detail', [
            'oplog' => new OperationalLogResource(
                $operationalLog->load([
                    'eventLogs' => function ($query) {
                        return $query->orderByDesc('time');
                    },
                    'personnelLogs' => function ($query) {
                        return $query->orderByDesc('time');
                    },
                    'facilityLogs' => function ($query) {
                        return $query->orderBy('name');
                    },
                    'reviewHistories' => function ($query) {
                        return $query->orderByDesc('reviewed_at');
                    }
                ])
            ),
            'logShifts' => new LogShiftCollection(LogShift::query()->get())
        ]);
    }

    public function review(OperationalLog $operationalLog, ReviewOperationalLogRequest $request)
    {
        $this->authorize('review-operational-logs', OperationalLog::class);
        $this->oplogService->review($operationalLog, $request);

        return redirect()
            ->route('operational-logs.show', $operationalLog->id)
            ->with('success', 'Your review has been submitted');
    }

    public function readyToReview(OperationalLog $operationalLog)
    {
        $this->authorize('switch-ready-to-review-operational-logs', OperationalLog::class);
        $this->oplogService->readyToReview($operationalLog);
    }

    public function export(OperationalLog $operationalLog)
    {
        $this->authorize('export-operational-logs', OperationalLog::class);
        return $this->oplogService->exportPdf($operationalLog);
    }


}
