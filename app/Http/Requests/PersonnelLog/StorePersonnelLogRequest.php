<?php

namespace App\Http\Requests\PersonnelLog;

use Illuminate\Foundation\Http\FormRequest;

class StorePersonnelLogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('edit-event-logs');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'operational_log' => ['required'],
            'personnel_logs' => ['required', 'array', 'min:1'],
            'personnel_logs.*.personnel_detail' => ['required'],
            'personnel_logs.*.time' => ['required']
        ];
    }
}
