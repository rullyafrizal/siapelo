<?php

namespace App\Http\Requests\EventLog;

use App\Enums\DateFormat;
use App\Models\OperationalLog;
use Illuminate\Foundation\Http\FormRequest;

class UpdateEventLogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('edit-event-logs');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'operational_log' => ['required'],
            'event_logs' => ['required', 'array', 'min:1'],
            'event_logs.*.time' => ['required', function ($attribute, $value, $fail) {
                $oplogId = request()->input('operational_log');
                $oplog = OperationalLog::query()
                    ->where('id', '=', $oplogId)
                    ->first();
                $time = to_carbon($value)->format(DateFormat::HOUR_MINUTE_SEC);

                if (!($time >= $oplog->shift_start_time && $time <= $oplog->shift_end_time)) {
                    $fail('The time only applies within current shift range time ' .
                        to_carbon($oplog->shift_start_time)->format(DateFormat::HOUR_MINUTE) . '-' .
                        to_carbon($oplog->shift_end_time)->format(DateFormat::HOUR_MINUTE)
                    );
                }
            }],
            'event_logs.*.specification' => ['required']
        ];
    }
}
