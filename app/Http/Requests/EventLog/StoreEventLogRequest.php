<?php

namespace App\Http\Requests\EventLog;

use Illuminate\Foundation\Http\FormRequest;

class StoreEventLogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('create-event-logs');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'operational_log' => ['required'],
            'event_logs' => ['required', 'array', 'min:1'],
            'event_logs.*.time' => ['required'],
            'event_logs.*.specification' => ['required']
        ];
    }
}
