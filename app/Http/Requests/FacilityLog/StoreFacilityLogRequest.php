<?php

namespace App\Http\Requests\FacilityLog;

use Illuminate\Foundation\Http\FormRequest;

class StoreFacilityLogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('edit-facilities-logs');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'operational_log' => ['required'],
            'facilities' => ['required', 'array', 'min:1'],
            'facilities.*.status' => ['required'],
            'facilities.*.name' => ['required'],
            'facilities.*.id' => ['required'],
            'facilities.*.description' => function ($attribute, $value, $fail) {
                $index = (int) explode('.', $attribute)[1];
                $status = request()->input('facilities')[$index]['status'];

                // Define the facility statuses that require a description
                $statusesWithDescription = ['FUNCTIONAL_WMI', 'NON_FUNCTIONAL', 'REQUIRES_MAINTENANCE', 'UNDER_REPAIR'];

                // Check if the current facility status requires a description
                if (in_array($status, $statusesWithDescription) && empty($value)) {
                    $fail('The description field is required for facilities with this status.');
                }
            }
        ];
    }
}
