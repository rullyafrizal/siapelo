<?php

namespace App\Http\Requests\OperationalLog;

use Illuminate\Foundation\Http\FormRequest;

class CreateOperationalLogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('create-operational-logs');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'log_date' => ['required', 'date'],
            'log_shift' => ['required']
        ];
    }
}
