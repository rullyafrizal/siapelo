<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('edit-user');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'min:1', 'max:255'],
            'last_name' => ['required', 'min:1', 'max:255'],
            'email' => ['required', 'min:1', 'max:255', Rule::unique('users')->ignore($this->user)],
            'role' => ['required'],
            'position' => ['required'],
            'password_old' => [
                'nullable',
                'required_with:password,password_confirmation',
                function ($attribute, $value, $fail) {
                    if (!empty($value) && !Hash::check($value, $this->user->password)) {
                        $fail("The old password is incorrect.");
                    }
                },
            ],
            'password' => ['sometimes', 'nullable', 'required_with:password_old,password_confirmation', 'confirmed', 'min:8', 'max:16'],
            'password_confirmation' => ['sometimes', 'nullable', 'required_with:password_old,password', 'same:password'],
        ];
    }
}
