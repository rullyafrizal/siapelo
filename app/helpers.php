<?php

use App\Enums\DateFormat;
use App\Enums\HttpStatus;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;

if(!function_exists('api_response')) {
    function api_response(int $status = HttpStatus::OK, string $message = '', $body = []): JsonResponse
    {
        if ($body) {
            return response()
                ->json([
                    'status' => $status,
                    'message' => $message,
                    'body' => $body
                ], $status);
        }

        return response()
            ->json([
                'status' => $status,
                'message' => $message,
            ], $status);
    }
}

if(!function_exists('to_carbon')) {
    function to_carbon($date)
    {
        if (!$date instanceof Carbon) {
            return Carbon::parse($date);
        }

        return $date;
    }
}

if (!function_exists('format_time')) {
    function format_time($time) {
        return to_carbon($time)->format(DateFormat::HOUR_MINUTE);
    }
}

if(!function_exists('dropColumnIfExist')) {
    function dropColumnIfExists($myTable, $column)
    {
        if (Schema::hasColumn($myTable, $column)) //check the column
        {
            Schema::table($myTable, function (Blueprint $table) use ($column)
            {
                $table->dropColumn($column); //drop it
            });
        }

    }
}

if (!function_exists('nowInHisFormat')) {
    function nowInHisFormat() {
        return Carbon::now()->format(DateFormat::HOUR_MINUTE_SEC);
    }
}
