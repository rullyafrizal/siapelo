export default {
  methods: {
    turnOnLoading(id) {
      this.loadingAction.id = id
      this.loadingAction.status = true
    },
    turnOffLoading() {
      this.loadingAction.id = null
      this.loadingAction.status = false
    },
  },
}
