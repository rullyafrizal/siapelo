import { createApp, h } from 'vue'
import { InertiaProgress } from '@inertiajs/progress'
import { createInertiaApp } from '@inertiajs/inertia-vue3'
import Auth from '@/Mixins/Auth'
import { plugin as formKitPlugin, defaultConfig } from '@formkit/vue'
import '@vuepic/vue-datepicker/dist/main.css'

InertiaProgress.init()

createInertiaApp({
  resolve: name => require(`./Pages/${name}`),
  title: title => title ? `${title} - SIAPELO` : 'SIAPELO',
  setup({ el, App, props, plugin }) {
    createApp({ render: () => h(App, props) })
      .use(plugin)
      .use(formKitPlugin, defaultConfig)
      .mixin(Auth)
      .mixin({ methods: { route: window.route } })
      .mount(el)
  },
})
