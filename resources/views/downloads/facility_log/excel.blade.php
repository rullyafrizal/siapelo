<table border="1">
    <thead>
    <tr>
        <th rowspan="3" style="text-align: center;"> <strong>Log Date</strong></th>
        <th rowspan="3" style="text-align: center;"> <strong>Log Shift</strong></th>
        <th rowspan="1" colspan="{{ count($data['headerFacilities']) * 2 }}" style="text-align: center;"> <strong>Facilities</strong></th>
    </tr>
    <tr>
        @foreach($data['headerFacilities'] as $headerFacility)
            <th colspan="2" style="text-align: center;"><strong>{{ $headerFacility }}</strong></th>
        @endforeach
    </tr>
    <tr>
        @foreach($data['headerFacilities'] as $headerFacility)
            <th style="text-align: center;"><strong>Status</strong></th>
            <th style="text-align: center;"><strong>Description</strong></th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($data['operationalLogs'] as $operationalLog)
        <tr>
            <td style="width: 100px;">{{ $operationalLog['logDate'] }}</td>
            <td style="width: 160px;">{{ $operationalLog['logShift'] }}</td>
            @foreach($operationalLog['facilityLogs'] as $facilityLog)
                <td style="width: 110px;">{{ $facilityLog['status'] }}</td>
                <td style="width: 100px;">{{ $facilityLog['description'] }}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
