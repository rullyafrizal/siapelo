<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <script>
        tailwind.config = {
            theme: {
                extend: {
                    colors: {
                        clifford: '#da373d',
                    }
                },
            }
        }
    </script>
    <style>
        @media print {
            body {
                font-family: sans-serif;
                -webkit-print-color-adjust:exact !important;
                print-color-adjust:exact !important;
            }
        }
    </style>
</head>
<body class="bg-gray-200">
<div id="printableArea" class="mx-16 my-16 p-16 shadow shadow-xl bg-white rounded-lg hover:shadow-2xl">
    <div class="flex justify-between">
        <div class="text-center">
            <h1 class="text-xl uppercase font-bold" style="vertical-align: bottom;">
                <img src="https://drive.google.com/uc?export=view&id=1vXg7D0ub2N0oxrZsMMgCBQVhakSyi4Ct" class="w-20 h-20 mx-auto mr-auto" alt="">
                Airnav Indonesia
            </h1>
        </div>
        <div class="text-center">
            <h1 class="text-3xl uppercase font-bold">ATS Operations</h1>
            <h1 class="text-3xl uppercase font-bold">Perum LPPNPI Kantor Cabang Pembantu</h1>
            <h1 class="text-3xl uppercase font-bold">Bandar Lampung</h1>
        </div>
        <div>&nbsp;</div>
    </div>
    <div class="flex justify-between mt-10">
        <div class="text-center">
            <p class="text-lg font-semibold">TWR/APP OPERATIONAL LOG</p>
        </div>
        <div class="text-center">
            <p class="text-lg font-regular">
                Date: <b>{{ $oplog['data']['logDate'] }}</b>&nbsp;&nbsp;&nbsp;Shift: <b>{{ $oplog['data']['logShift'] }}</b>
            </p>
        </div>
    </div>
    <div class="mt-5">
        <table class="border-collapse border border-black w-full">
            <thead>
            <tr>
                <th class="border border-black bg-gray-300 text-black" colspan="3">
                    Personnel On Duty
                </th>
            </tr>
            <tr>
                <th class="border border-black bg-gray-300" style="width: 50%">
                    Name
                </th>
                <th class="border border-black bg-gray-300" style="width: 30%">
                    Clock In Time
                </th>
                <th class="border border-black bg-gray-300" style="width: 20%">
                    Sign
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($oplog['data']['personnelLogs'] as $personnelLog)
                <tr>
                    <td class="border-r border-black" style="vertical-align: top;">
                        <div class="ml-2">
                            {{ $personnelLog['name'] }}
                        </div>
                    </td>
                    <td class="border-r border-black text-center" style="vertical-align: top;">
                        <div class="ml-2">
                            {{ $personnelLog['time'] }}
                        </div>
                    </td>
                    <td class="text-center" style="vertical-align: top;">
                        @if($personnelLog['sign'])
                            <div class="ml-2">
                                &#x2713;
                            </div>
                        @else
                            <div class="ml-2">
                                -
                            </div>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="mt-5">
        <table class="border-collapse border border-black w-full">
            <thead>
            <tr>
                <th class="border border-black bg-gray-300 text-black" colspan="2">Event Logs</th>
            </tr>
            <tr>
                <th class="border border-black bg-gray-300">Time</th>
                <th class="border border-black bg-gray-300">Specification</th>
            </tr>
            </thead>
            <tbody>
            @foreach($oplog['data']['eventLogs'] as $eventLog)
                <tr>
                    <td class="border-r border-black text-center" style="width: 15%; vertical-align: top;">
                        <div class="ml-2">
                            {{ $eventLog['time'] }}
                        </div>
                    </td>
                    <td>
                        <div class="ml-2">
                            {{ $eventLog['specification'] }}
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="mt-5">
        <table class="border-collapse border border-black w-full">
            <thead>
            <tr>
                <th class="border border-black bg-gray-300 text-black" colspan="99">Facilities Condition</th>
            </tr>
            <tr>
                <th class="border border-black bg-gray-300">Name</th>
                <th class="border border-black bg-gray-300">Status</th>
                <th class="border border-black bg-gray-300">Description</th>
            </tr>
            </thead>
            <tbody>
                @foreach($oplog['data']['facilities'] as $facility)
                    <tr class="my-2">
                        <td class="border border-black" width="25%">
                            <div class="ml-2">
                                {{ $facility['name'] }}
                            </div>
                        </td>
                        <td class="border border-black text-center" width="25%">
                            {{ $facility['status'] ?? '-' }}
                        </td>
                        <td class="border border-black" width="50%">
                            <div class="ml-2">
                                {{ $facility['description'] ?? '-' }}
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="mt-5 flex justify-end">
        <div class="text-center">
            <p class="">ATS Operation Manager</p>
            <img style="display: block; margin: 0 auto;" width="200px" height="200px"
                 src="{{ $oplog['data']['reviewerSignature'] }}" alt="Reviewer Signature">
            <p>{{ $oplog['data']['reviewerName'] }}</p>
            <p class="text-xs mt-2 font-thin">Signed and approved at {{ $oplog['data']['reviewedAt'] }}</p>
        </div>
    </div>
</div>
<div onclick="print('printableArea')" class="fixed bottom-4 right-4 bg-indigo-600 px-4 py-3 font-semibold rounded-xl shadow shadow-lg text-white hover:bg-orange-400 cursor-pointer text-center">
    <button type="button">Print Me</button>
</div>
</body>
<script>

    const print = (divId) => {
        let printContents = document.getElementById(divId).innerHTML;
        let originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
</html>
